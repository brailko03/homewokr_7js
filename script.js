
let array = ["hello", "world", 23, "23", null];
function filterBy(arr, arrType) {
  return arr.filter((n) => typeof n !== arrType);
}
let newArr = filterBy(array, "string");
console.log(newArr);
